<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\DB;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/consultas', function (){
    /* 
    return DB::table('users')->where('name', '%fer%')->get();
    return DB::table('users')->where([['email', 'like', '%laravel%'], ['email', 'like', '%com']])->get();
    return DB::table('users')->where('email', 'like', '%laravel%')->orWhere('email', 'like', '%com')->get();
    return DB::table('users')->insert(['name' => 'nombre', 'email' => 'email', 'password' => 'contraseña']);
    return DB::table('users')->insertGetId(['name' => 'nombre', 'email' => 'email', 'password' => 'contraseña']);
    return DB::table('users')->where('id', 2)->update(['email' => 'nuevoemail']);
    return DB::table('users')->where('id', 3)->delete();
    */
})->name('consulta');
